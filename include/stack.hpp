#ifndef STACK_H
#define STACK_H
#include <iostream>

namespace Ds
{
    enum class stackerrorcode
    {
        NoError = 0,
        StackEmpty = 100,
        StackFull = 101,
        MemoryAllocFail = 102,
        Duplicate = 103,
    };

    template <class T>
    struct Node
    {
        T data;
        Node* next;
    };

    template <class T, int SIZE = 5>
    class stack
    {
        public:
            stack();
            virtual ~stack();
            T& getTop();
            int& getStackSize();
            stackerrorcode push(const T& p_data, bool p_checkDuplicate = false);
            stackerrorcode pop();
            bool isEmpty();
            bool isFull();
            void print();
            static std::string strError(const stackerrorcode& errorcode);
            bool isDuplicate(const T& p_data);
            void resize(const int& size);

        private:
            bool find(const T& p_data);

            //Member variables
            int m_maxSize = SIZE;
            T m_top;
            int m_stackSize = 0;
            Node<T>* m_head = nullptr;
    };

////////// Template implementation //////////

    template <class T, int SIZE>
    Ds::stack<T, SIZE>::stack()
    {
        //ctor
    }

    template <class T, int SIZE>
    Ds::stack<T, SIZE>::~stack()
    {
        //dtor
    }

    template <class T, int SIZE>
    T& Ds::stack<T, SIZE>::getTop()
    {
        if(m_head != nullptr)
        {
            m_top = m_head->data;
        }
        return m_top;
    }

    template <class T, int SIZE>
    int& Ds::stack<T, SIZE>::getStackSize()
    {
        return m_stackSize;
    }

    //push an element into stack
    template <class T, int SIZE>
    Ds::stackerrorcode Ds::stack<T, SIZE>::push(const T& p_data, bool p_checkDuplicate)
    {
        if(p_checkDuplicate)
        {
           if(isDuplicate(p_data))
           {
               return stackerrorcode::Duplicate;
           }
        }

        if(isFull())
        {
            return stackerrorcode::StackFull;
        }

        Ds::Node<T>* node = new Ds::Node<T>();

        if(node == nullptr)
        {
           return stackerrorcode::MemoryAllocFail;
        }
        node->data = p_data;

        if(m_head == nullptr)
        {
           node->next = nullptr;
           m_head = node;
        }
        else
        {
           node->next = m_head;
           m_head = node;
        }

        //Increment stack size
        m_stackSize++;

        return stackerrorcode::NoError;
    }

    //pop the element from the list
    template <class T, int SIZE>
    stackerrorcode Ds::stack<T, SIZE>::pop()
    {
        Ds::Node<T>* temp = nullptr;

        if(isEmpty())
        {
            return stackerrorcode::StackEmpty;
        }

        temp = m_head;
        m_head = m_head->next;

        //delete node
        delete temp;
        temp = nullptr;

        m_stackSize--;

        return stackerrorcode::NoError;
    }

    template <class T, int SIZE>
    bool Ds::stack<T, SIZE>::isEmpty()
    {
        if(m_stackSize)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    template <class T, int SIZE>
    bool Ds::stack<T, SIZE>::isFull()
    {
        if(m_stackSize < m_maxSize)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    template <class T, int SIZE>
    void Ds::stack<T, SIZE>::print()
    {
        Ds::Node<T>* temp = m_head;
        if(isEmpty())
        {
            return;
        }

        while(temp != nullptr)
        {
            std::cout << temp->data << std::endl;
            temp = temp->next;
        }
    }

    template <class T, int SIZE>
    bool Ds::stack<T, SIZE>::isDuplicate(const T& p_data)
    {
        return find(p_data);
    }

    //Linear search algorithm - Time complexity O(n).
    //Todo : Replace it with an efficient algorithm.
    template <class T, int SIZE>
    bool Ds::stack<T, SIZE>::find(const T& p_data)
    {
        Node<T>* temp = nullptr;
        temp = m_head;

        while(temp != nullptr)
        {
            if(temp->data == p_data)
            {
                return true;
            }
            temp = temp->next;
        }
        return false;
    }

    template <class T, int SIZE>
    std::string Ds::stack<T, SIZE>::strError(const stackerrorcode& errorcode)
    {
        switch(errorcode)
        {
            case stackerrorcode::NoError:
                return "No error";
            case stackerrorcode::StackEmpty:
                return "Stack is Empty";
            case stackerrorcode::StackFull:
                return "Stack is Full";
            case stackerrorcode::MemoryAllocFail:
                return "Memory allocation failed";
            case stackerrorcode::Duplicate:
                return "Duplicate element";
            default:
                return "Unknown error";
        }
    }

    template <class T, int SIZE>
    void Ds::stack<T, SIZE>::resize(const int& size)
    {
        m_maxSize = size;
    }
} //namespace Ds end
#endif // STACK_H
