#include <iostream>
#include <string>
#include "stack.h"
//using namespace std;
struct testData
{
    std::string name;
    short id;
};

int main()
{
    std::cout << "######### Stack Data Structure Test #########" << std::endl;

    Ds::stack<int, 10>* myStack = new Ds::stack<int, 10>();

    Ds::stackerrorcode errcode;

    std::cout << "#### Test Push Operation ###" << std::endl;

    std::cout << "Stack Size:" << myStack->getStackSize() << "" << "Top:" << myStack->getTop() << std::endl;

    //push
    myStack->push(10);
    myStack->push(20);

    std::cout << "#### Test Check Duplicate Operation ###" << std::endl;

    //Enable check for duplicate data
   if( Ds::stackerrorcode::Duplicate == myStack->push(20, true))
   {
       std::cout << "The element already exists in stack...!! Aborting push operation...!!" << std::endl;
   }

    //print stack elements
    myStack->print();

    std::cout << "Stack Size:" << myStack->getStackSize() << "" << "Top:" << myStack->getTop() << std::endl;

    std::cout << "#### Test Pop Operation ###" << std::endl;
    //pop
    myStack->pop();

    std::cout << "Stack Size:" << myStack->getStackSize() << "" << "Top:" << myStack->getTop() << std::endl;

    std::cout << "#### Test Push string Operation ###" << std::endl;

    Ds::stack<std::string>* str = new Ds::stack<std::string>();

    str->push("testString");

    str->print();

    std::cout << "#### Test maximum size and resize Operations ###" << std::endl;

    for(int i = 0; i < 5; i++)
    {
        errcode = myStack->push(i);

        if(errcode != Ds::stackerrorcode::NoError)
        {
            std::cout << Ds::stack<int>::strError(errcode) << std::endl;

            if(errcode == Ds::stackerrorcode::StackFull)
            {
                std::string confirm;
                std::cout << "Do you want to resize ?" ;
                std::cin >> confirm;

                if(confirm == "yes")
                {
                    int size = 0;
                    std::cout << "Size:" ;
                    std::cin >> size;
                    myStack->resize(size);

                    //try push operation of current element and continue the loop
                    myStack->push(i);
                    continue;
                }
            }
        }
    }

    myStack->print();


    return 0;
}
